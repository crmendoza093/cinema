# README

Application to reserve movies.


`bundle install`

### Ruby version

2.6.3

### Rails version

6.0.2

### System dependencies

`gem 'faker'`

### Database creation


`rake db:setup`

`rake db:migrate`



### Database initialization

`rake db:seed`
