class CreateReserves < ActiveRecord::Migration[6.0]
  def change
    create_table :reserves do |t|
      t.date :date
      t.integer :movie_id
      t.integer :customer_id

      t.timestamps
    end
  end
end
