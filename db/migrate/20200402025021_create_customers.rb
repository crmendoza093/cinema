class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :email
      t.integer :identification
      t.integer :cellphone

      t.timestamps
    end
  end
end
