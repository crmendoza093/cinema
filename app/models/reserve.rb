class Reserve < ApplicationRecord
    validates :date, presence: true
    validates :movie_id, presence: true, numericality: true
    validates :customer_id, presence: true, numericality: true
end
