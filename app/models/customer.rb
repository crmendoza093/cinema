class Customer < ApplicationRecord
  validates :name, presence: true
  validates :email, presence: true
  validates :identification, presence: true, numericality: true
  validates :cellphone, presence: true, numericality: true
end
