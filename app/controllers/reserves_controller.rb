class ReservesController < ApplicationController
  before_action :set_reserf, only: [:show, :edit, :update, :destroy]

  def index
    @reserves = Reserve.all
    @reserf = Reserve.new
  end

  def new
    @reserf = Reserve.new
  end

  def create
    @reserf = Reserve.new(reserf_params)

    respond_to do |format|
      if @reserf.save
        format.html { redirect_to @reserf, notice: 'Pelicula reservada' }
      end
    end
  end

  private
    def set_reserf
      @reserf = Reserve.find(params[:id])
    end

    def reserf_params
      params.require(:reserf).permit(:date, :movie_id, :customer_id)
    end
end
