json.extract! reserf, :id, :date, :movie_id, :customer_id, :created_at, :updated_at
json.url reserf_url(reserf, format: :json)
